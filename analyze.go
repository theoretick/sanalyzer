package main

import (
	"io"
	"os"
	"os/exec"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

const (
	pathSemGrep   = "/root/.local/bin/semgrep"
	semgrepPlugin = "/root/semgrep-rules/javascript/lang/security"
	pathOutput    = "/root/output.json"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	setupCmd(exec.Command("touch", pathOutput)).Run()

	// semgrep --config "https://semgrep.dev/p/eslint-plugin-security"
	cmd := exec.Command(
		pathSemGrep,
		"--disable-version-check",
		"--strict",
		"-q",
		"--json",
		"--output",
		pathOutput,
		"--config",
		semgrepPlugin,
	)
	log.Debugf("%s", cmd.String())

	err := setupCmd(cmd).Run()

	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			if waitStatus.ExitStatus() >= 2 {
				log.Errorf("An error occurred while running semgrep: %s\n", err)
				return nil, err
			}
		}
	}

	return os.Open(pathOutput)
}
