package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/semgrep/v2/plugin"
)

func init() {
	log.SetFormatter(&logutil.Formatter{Project: "semgrep"})
}

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "semgrep analyzer for GitLab SAST"
	app.Author = "GitLab"

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
