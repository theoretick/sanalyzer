```
❯ docker run --rm -it --env SECURE_LOG_LEVEL=debug --env CI_PROJECT_DIR=/tmp/project --volume "$PWD/test/fixtures":/tmp/project analyzer
[INFO] [semgrep] [2020-10-07T21:26:01Z] ▶ Detecting project
[INFO] [semgrep] [2020-10-07T21:26:01Z] ▶ Found project in /tmp/project

[INFO] [semgrep] [2020-10-07T21:26:01Z] ▶ Running analyzer
[DEBU] [semgrep] [2020-10-07T21:26:01Z] ▶ /root/.local/bin/semgrep --disable-version-check --strict -q --json --output /root/output.json --config /root/semgrep-rules/javascript/lang/security
{"results": [{"check_id": "root.semgrep-rules.javascript.lang.security.detect-eval-with-expression", "path": "main.js", "start": {"line": 10, "col": 1}, "end": {"line": 10, "col": 13}, "extra": {"message": "Detected eval(variable), which could allow a malicious actor to run arbitrary code.\n", "metavars": {"$OBJ": {"start": {"line": 10, "col": 6, "offset": 240}, "end": {"line": 10, "col": 12, "offset": 246}, "abstract_content": "myeval", "unique_id": {"type": "id", "value": "myeval", "kind": "Global", "sid": 1}}}, "metadata": {"cwe": "CWE-95: Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')", "owasp": "A1: Injection", "source-rule-url": "https://github.com/nodesecurity/eslint-plugin-security/blob/master/rules/detect-eval-with-expression.js"}, "severity": "WARNING", "lines": "eval(myeval);"}}], "errors": []}
[INFO] [semgrep] [2020-10-07T21:26:02Z] ▶ Creating report
```

```
❯ cat test/fixtures/gl-sast-report.json
{
  "version": "3.0",
  "vulnerabilities": [
    {
      "id": "82469859d2908f6a842ea732083f4d0477f3420ec5535cdbf57f63eb043ddf19",
      "category": "",
      "message": "root.semgrep-rules.javascript.lang.security.detect-eval-with-expression",
      "cve": "",
      "scanner": {
        "id": "",
        "name": ""
      },
      "location": {},
      "identifiers": null
    }
  ],
  "remediations": [],
  "scan": {
    "scanner": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "type": ""
  }
}
```