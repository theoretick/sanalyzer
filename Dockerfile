FROM golang:1.14 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM returntocorp/semgrep-action:v1

COPY --from=build --chown=root:root /go/src/app/analyzer /

# /root/.local/bin/semgrep --disable-version-check --json --config https://semgrep.dev/p/eslint-plugin-security

WORKDIR /root
RUN git clone https://github.com/returntocorp/semgrep-rules.git semgrep-rules
ENTRYPOINT []
CMD ["/analyzer", "run"]
