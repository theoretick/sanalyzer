package main

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const toolID = "XXX" // TODO

// SemgrepReport ...
type SemgrepReport struct {
	Errors  []SemgrepError  `json:"errors"`
	Results []SemgrepResult `json:"results"`
}

// SemgrepError ...
type SemgrepError struct {
	Data    string `json:"data"`
	Message string `json:"message"`
}

// SemgrepResult ...
type SemgrepResult struct {
	CheckID string `json:"check_id"`
}

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var out SemgrepReport

	err := json.NewDecoder(reader).Decode(&out)
	if err != nil {
		return nil, err
	}

	// Create issues from the out
	issues := make([]issue.Issue, len(out.Results))
	for i, result := range out.Results {
		issues[i] = issue.Issue{
			Message: result.CheckID,
		}
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}
